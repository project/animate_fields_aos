<?php

namespace Drupal\animate_fields_aos;

/**
 * Gathers and provides the data-aos-* attributes that can be used into fields.
 */
interface AttributesManagerInterface {

  /**
   * The stored value representing "no data animation".
   */
  const NO_DATA_ANIMATION_VALUE = 'none';

  /**
   * Get the animation options for data-aos attribute.
   *
   * @return array
   *   An array of tags.
   */
  public function getAnimationOptions();

  /**
   * Get the animation options for data-aos-anchor-placement attribute.
   *
   * @return array
   *   An array of tags.
   */
  public function getAnchorPlacementOptions();

  /**
   * Get the animation options for data-aos-easing attribute.
   *
   * @return array
   *   An array of tags.
   */
  public function getEasingOptions();

}
