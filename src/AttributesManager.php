<?php

namespace Drupal\animate_fields_aos;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Plugin\Discovery\YamlDiscovery;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Component\Plugin\Discovery\CachedDiscoveryInterface;
use Drupal\Core\Plugin\Discovery\ContainerDerivativeDiscoveryDecorator;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Gathers and provides the tags that can be used to wrap fields.
 */
class AttributesManager extends DefaultPluginManager implements AttributesManagerInterface, PluginManagerInterface, CachedDiscoveryInterface {

  use StringTranslationTrait;

  /**
   * The theme handler.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * {@inheritdoc}
   */
  protected $defaults = [
    'label' => '',
    'group' => '',
    'descripton' => '',
  ];

  /**
   * Constructs a new AttributesManager instance.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $theme_handler
   *   The theme handler.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend.
   */
  public function __construct(ModuleHandlerInterface $module_handler, ThemeHandlerInterface $theme_handler, CacheBackendInterface $cache_backend) {
    $this->moduleHandler = $module_handler;
    $this->themeHandler = $theme_handler;
    $this->setCacheBackend($cache_backend, 'animate_fields_aos', ['animate_fields_aos']);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDiscovery() {
    if (!isset($this->discovery)) {
      $this->discovery = new YamlDiscovery('options', $this->moduleHandler->getModuleDirectories() + $this->themeHandler->getThemeDirectories());
      $this->discovery = new ContainerDerivativeDiscoveryDecorator($this->discovery);
    }
    return $this->discovery;
  }

  /**
   * {@inheritdoc}
   */
  public function getAnimationOptions() {
    $options = [
      AttributesManagerInterface::NO_DATA_ANIMATION_VALUE => $this->t('None'),
    ];

    $effect_groups = ['Fade', 'Flip', 'Slide', 'Zoom'];
    foreach ($this->getDefinitions() as $id => $definition) {
      if (in_array($definition['group'], $effect_groups)) {
        $options[$definition['group']][$id] = $this->t('@label (@tag)', [
          '@label' => $definition['label'],
          '@tag' => $id,
        ]);
      }
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function getAnchorPlacementOptions() {
    $options = [
      AttributesManagerInterface::NO_DATA_ANIMATION_VALUE => $this->t('None'),
    ];

    foreach ($this->getDefinitions() as $id => $definition) {
      if ($definition['group'] === 'Anchor Placement') {
        $options[$id] = $this->t('@label (@tag)', [
          '@label' => $definition['label'],
          '@tag' => $id,
        ]);
      }
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function getEasingOptions() {
    $options = [
      AttributesManagerInterface::NO_DATA_ANIMATION_VALUE => $this->t('None'),
    ];

    foreach ($this->getDefinitions() as $id => $definition) {
      if ($definition['group'] === 'Ease') {
        $options[$id] = $this->t('@label (@tag)', [
          '@label' => $definition['label'],
          '@tag' => $id,
        ]);
      }
    }

    return $options;
  }

}
