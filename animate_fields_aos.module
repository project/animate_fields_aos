<?php

/**
 * @file
 * Provides functionality and hooks.
 */

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FormatterInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_field_formatter_third_party_settings_form().
 */
function animate_fields_aos_field_formatter_third_party_settings_form(FormatterInterface $plugin, FieldDefinitionInterface $field_definition, $view_mode, $form, FormStateInterface $form_state) {
  if (!(\Drupal::currentUser()->hasPermission('edit animate fields formatter settings'))) {
    // Current user doesn't have permission to edit animate formatter settings.
    return;
  }

  $settings['animate_fields_aos'] = [];

  $settings['animate_fields_aos'] = [
    '#type' => 'details',
    '#title' => t('AOS Animations'),
  ];

  $settings['animate_fields_aos']['display_animations'] = [
    '#type' => 'checkbox',
    '#title' => t('Display Animations'),
    '#default_value' => $plugin->getThirdPartySetting('animate_fields_aos', 'display_animations', FALSE),
  ];

  $visibility_condition = [
    'visible' => [
      ':input[id*="animate-fields-aos-display-animations"]' => ['checked' => TRUE],
    ],
  ];

  $settings['animate_fields_aos']['options'] = [
    '#type' => 'container',
    '#attributes' => [
      'class' => [
        'animate_fields_aos--options',
      ],
    ],
    '#states' => $visibility_condition,
  ];

  /** @var \Drupal\animate_fields_aos\AttributesManager $animation_options */
  $animation_options = \Drupal::service('animate_fields_aos.attributes_manager')->getAnimationOptions();
  $settings['animate_fields_aos']['animation'] = [
    '#title' => t('Animation'),
    '#type' => 'select',
    '#options' => $animation_options,
    '#default_value' => $plugin->getThirdPartySetting('animate_fields_aos', 'animation', 'fade'),
    '#description' => t('Default value: Fade (fade)'),
    '#states' => $visibility_condition,
  ];

  /** @var \Drupal\animate_fields_aos\AttributesManager $anchor_placement_options */
  $anchor_placement_options = \Drupal::service('animate_fields_aos.attributes_manager')->getAnchorPlacementOptions();
  $settings['animate_fields_aos']['anchor_placements'] = [
    '#title' => t('Anchor placements'),
    '#type' => 'select',
    '#options' => $anchor_placement_options,
    '#default_value' => $plugin->getThirdPartySetting('animate_fields_aos', 'anchor_placements', 'top-bottom'),
    '#description' => t('Default value: Top Bottom (top-bottom)'),
    '#states' => $visibility_condition,
  ];

  /** @var \Drupal\animate_fields_aos\AttributesManager $easing_options */
  $easing_options = \Drupal::service('animate_fields_aos.attributes_manager')->getEasingOptions();
  $settings['animate_fields_aos']['easing'] = [
    '#title' => t('Easing'),
    '#type' => 'select',
    '#options' => $easing_options,
    '#default_value' => $plugin->getThirdPartySetting('animate_fields_aos', 'easing', 'ease'),
    '#description' => t('Default value: Ease (ease)'),
    '#states' => $visibility_condition,
  ];

  $settings['animate_fields_aos']['offset'] = [
    '#type' => 'number',
    '#title' => t('Offset'),
    '#default_value' => $plugin->getThirdPartySetting('animate_fields_aos', 'offset', 120),
    '#description' => t('Offset (in px) from the original trigger point. Default value: 120'),
    '#min' => 0,
    '#states' => $visibility_condition,
  ];

  $settings['animate_fields_aos']['delay'] = [
    '#type' => 'number',
    '#title' => t('Delay'),
    '#default_value' => $plugin->getThirdPartySetting('animate_fields_aos', 'delay', 0),
    '#description' => t('Values from 0 to 3000, with step 50ms. Default value: 0'),
    '#min' => 0,
    '#max' => 3000,
    '#states' => $visibility_condition,
  ];

  $settings['animate_fields_aos']['duration'] = [
    '#type' => 'number',
    '#title' => t('Duration'),
    '#default_value' => $plugin->getThirdPartySetting('animate_fields_aos', 'duration', 400),
    '#description' => t('Values from 0 to 3000, with step 50ms. Default value: 400'),
    '#min' => 0,
    '#max' => 3000,
    '#states' => $visibility_condition,
  ];

  $settings['animate_fields_aos']['mirror'] = [
    '#type' => 'checkbox',
    '#title' => t('Mirror'),
    '#default_value' => $plugin->getThirdPartySetting('animate_fields_aos', 'mirror', FALSE),
    '#description' => t('Whether elements should animate out while scrolling past them. Default value: unchecked'),
    '#states' => $visibility_condition,
  ];

  return $settings['animate_fields_aos'];
}

/**
 * Implememts hook_preprocess_field().
 */
function animate_fields_aos_preprocess_field(&$vars) {
  if (isset($vars['element']['#third_party_settings']['animate_fields_aos'])) {
    $animate_fields_aos_config = $vars['element']['#third_party_settings']['animate_fields_aos'];

    // Whether Animate Fields AOS data attributes should be inserted.
    if (!empty($animate_fields_aos_config['display_animations'])) {

      // Add data-aos attribute from setting:
      if (!empty($animate_fields_aos_config['animation'])) {
        $vars['attributes']['data-aos'] = $animate_fields_aos_config['animation'];
      }

      // Add data-aos-anchor-placement attribute from setting:
      if (!empty($animate_fields_aos_config['anchor_placements'])) {
        $vars['attributes']['data-aos-anchor-placement'] = $animate_fields_aos_config['anchor_placements'];
      }

      // Add data-aos-easing attribute from setting:
      if (!empty($animate_fields_aos_config['easing'])) {
        $vars['attributes']['data-aos-easing'] = $animate_fields_aos_config['easing'];
      }

      // Add data-aos-offset attribute from setting:
      if (!empty($animate_fields_aos_config['offset'])) {
        $vars['attributes']['data-aos-offset'] = $animate_fields_aos_config['offset'];
      }

      // Add data-aos-delay attribute from setting:
      if (!empty($animate_fields_aos_config['delay'])) {
        $vars['attributes']['data-aos-delay'] = $animate_fields_aos_config['delay'];
      }

      // Add data-aos-duration attribute from setting:
      if (!empty($animate_fields_aos_config['duration'])) {
        $vars['attributes']['data-aos-duration'] = $animate_fields_aos_config['duration'];
      }

      // Add data-aos-mirror attribute from setting:
      if (!empty($animate_fields_aos_config['mirror'])) {
        $vars['attributes']['data-aos-mirror'] = $animate_fields_aos_config['mirror'];
      }

      $vars['#attached']['library'][] = 'animate_fields_aos/animate_fields_aos';
    }
  }
}

/**
 * Implements hook_help().
 */
function animate_fields_aos_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.animate_fields_aos':
      if (file_exists(__DIR__ . "/README.md")) {
        $content = file_get_contents(__DIR__ . "/README.md");
        if ($content !== FALSE) {
          // If the Markdown module is not available, just output the text
          // wrapped in a PRE tag.
          if (!\Drupal::moduleHandler()->moduleExists('markdown')) {
            return '<pre>' . $content . '</pre>';
          }
          // Use the Markdown filter.
          else {
            $filter_manager = \Drupal::service('plugin.manager.filter');
            $settings = \Drupal::configFactory()->get('markdown.settings')->getRawData();
            $config = ['settings' => $settings];
            $filter = $filter_manager->createInstance('markdown', $config);
            return $filter->process($content, 'en');
          }
        }
      }
      else {
        return '<p> Help is not available for this module </p>';
      }
    default:
      // No help found for the given route name.
      return [];
  }
}
