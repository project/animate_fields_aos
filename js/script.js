/**
 * @file
 * JavaScript file for the animate_fields_aos module.
 */

(function ($, Drupal) {

  Drupal.behaviors.animateFieldsAos = {
    attach: function attach(context, settings) {
      AOS.init();
    }
  };

})(jQuery, Drupal);
