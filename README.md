# Animate Fields - AOS 🚀

## CONTENT OF THIS FILE

  - Introduction
  - Requirements
  - Installation
  - Configuration
  - Maintainers
  - Links
  - Related modules
  - Support
## INTRODUCTION

The module allows a site builder to display fields with animations while scrolling down and up. This module utilize [AOS(Animate On Scroll)](https://michalsnik.github.io/aos) library. The animation options are available in the Manage Display form for Content, Media and Block Types.


## REQUIREMENTS

This module requires the following javascript library which 
is downloaded and installed automatically by the module:

- [AOS(Animate On Scroll)](https://michalsnik.github.io/aos)


## INSTALLATION

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## CONFIGURATION
1. Download, install and enable the Animate Fields - AOS module as normal.
1. Got to any Content/Media/Block types → Manage Display and click on the
gear icon of the desired field to animate.
1. Click on `AOS Animation` details options.
1. Check the `Display Animations` checkbox.
1. Select the animation and its variations to apply.
1. Clear all cache, refresh the page where you expect to see the field and
its animation.
1. Enjoy them.


## MAINTAINERS

- Carlos Puello - [carma03](https://www.drupal.org/u/carma03)


### Links
- [AOS Library](https://michalsnik.github.io/aos)

### Related Modules
- [Advanced animate on scroll](https://www.drupal.org/project/adv_animate_on_scroll)

### Support
For bug reports and feature requests please use the Drupal.org issue tracker:
https://drupal.org/project/issues/animate_fields_aos.
